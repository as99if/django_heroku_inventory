from rest_framework import routers
from .api import ProductViewSet
from django.urls import path
from django.conf.urls import include

ROUTER = routers.DefaultRouter()
ROUTER.register('api/products', ProductViewSet, 'products')

urlpatterns = [
    path('', include(ROUTER.urls))
]