from .models import Product
from rest_framework import viewsets, permissions
from .serializers import ProductSerializer

# Product viewset - CRUD api
class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

